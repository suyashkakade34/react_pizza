import { useEffect, useState } from "react";
import "../components/bill.css"
import { OrderDetails } from "../utils/OrderDetails";

interface BillProps{
    data:OrderDetails|undefined;
}

const Bill:React.FC<BillProps>=({data})=>{




  let formattedDateString=""
  let formattedTimeString=""
  if(data?.address!==undefined){
    const dateObject = new Date(data?.date);

  const day = dateObject.getDate();
  const month = dateObject.getMonth() + 1; // Months are 0-indexed, so adding 1
  const year = dateObject.getFullYear();
  const hours = dateObject.getHours();
  const minutes = dateObject.getMinutes();
  
  const timePeriod = hours >= 12 ? 'PM' : 'AM';
  const formattedHours = hours % 12 || 12; // Convert 24-hour format to 12-hour format
  
   formattedDateString = `${day < 10 ? '0' : ''}${day}-${month < 10 ? '0' : ''}${month}-${year}`;
   formattedTimeString = `${formattedHours}:${minutes < 10 ? '0' : ''}${minutes}${timePeriod}`;
  }
  const [taxable,setTaxable]=useState(0)
  const [finalAmount,setFinalAmount]=useState(0)
  const tax=()=>{
      if(data?.totalAmount!==undefined){
        setTaxable(data.totalAmount*0.18)
          return taxable;
      }else{
        return 0;
      }
  }
  const grandTotal=()=>{
    if(data?.totalAmount!==undefined){
      setFinalAmount(data.totalAmount+taxable)
    }else{
      return 0
    }
  }
  useEffect(()=>{
    tax()
    grandTotal()
  },)


   return(
    <div> 
       <table className="t1">
       <tr>
         {/* invoice number */}
         <th colSpan={1}>Order ID: <span>{data?.orderId}</span> </th>
         {/* date */}
         <th colSpan={3}>{formattedDateString}{formattedTimeString}</th>
       </tr>
       <tr>
         <td colSpan={2}>
           <strong>Pay To:</strong> <br/> Suyshaa's Pizza <br/>
           301 B2 Cerebrum Tech Park <br/>
           
         </td>
         <td colSpan={2}>
           <strong>Customer:</strong> <br/>
           {data?.firstName} {data?.lastName} <br/>
           {data?.address} <br/>
           <br />
         </td>
       </tr>
       <tr>
         <th>Name/Description</th>
         <th>Qty.</th>
         <th>Size</th>
         <th>Amount</th>
       </tr>
       {data?.pizza.map(pizzas=>(
         <tr>
         <td>{pizzas.pizzaName}</td>
         <td>{pizzas.quantity}</td>
         <td>{pizzas.size}</td>
         <td>{pizzas.subTotal}</td>
       </tr>
       ))}
       <tr>
         <th colSpan={3}>Subtotal:</th>
         <td>{data?.totalAmount}</td>
       </tr>
       <tr>
         <th colSpan={2}>Tax</th>
         <td>18%</td>
         <td>{taxable.toFixed()}</td>
       </tr>
       <tr>
         <th colSpan={3}>Grand Total:</th>
         <td>Rs {finalAmount.toFixed()}</td>
       </tr>
         </table>
        
      
    </div>
   )
}


export default Bill;