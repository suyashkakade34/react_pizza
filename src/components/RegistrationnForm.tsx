import React, {useState} from 'react';
import './registration.css'
function RegistrationForm() {
    
    const [firstName, setFirstName] = useState('');
    const [passwordType, setPasswordType] = useState("password");
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [password,setPassword] = useState('');
    const [confirmPassword,setConfirmPassword] = useState('');
    const [confirm,setConfirm]=useState("password")

    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {id , value} = e.target;
        if(id === "firstName"){
            setFirstName(value);
            console.log(firstName);
            
        }
        if(id === "lastName"){
            setLastName(value);
        }
        if(id === "email"){
            setEmail(value);
        }
        if(id === "password"){
            setPassword(value);
        }
        if(id === "confirmPassword"){
            setConfirmPassword(value)
           
        }

    }

    const handleSubmit  = () => {
            console.log(firstName,lastName,email,password,confirmPassword);
        
    }
    const togglePassword =()=>{
        if(passwordType==="password")
        {
         setPasswordType("text")
         return;
        }
        setPasswordType("password")
      }
      const showConfirm =()=>{
        if(confirm==="password")
        {
         setConfirm("text")
         return;
        }
        setConfirm("password")
      }

    return(
        <form onSubmit={handleSubmit}>
            <div className="form">
            <h1>Register Here</h1>
            <div className="form-body">
                <div className="username">
                    <label className="form-label" htmlFor="firstName">First Name </label>
                    <input className="form-control" type="text" value={firstName} onChange = {(e) => handleInputChange(e)} id="firstName" placeholder="First Name"/>
                </div>
                <div className="lastname">
                    <label className="form-label" htmlFor="lastName">Last Name </label>
                    <input required type="text" name="" id="lastName" value={lastName}  className="form-control" onChange = {(e) => handleInputChange(e)} placeholder="LastName"/>
                </div>
                <div className="email">
                    <label className="form-label" htmlFor="email">Email </label>
                    <input  type="email" id="email" className="form-control" value={email} onChange = {(e) => handleInputChange(e)} placeholder="Email"/>
                </div>
                <div className="password">
                    <label className="form-label" htmlFor="password">Password </label>
                    <div style={{display:'flex'}}>
                    <div style={{width:'1000px'} }>
                    <input className="form-control" type={passwordType}  id="password" value={password} onChange = {(e) => handleInputChange(e)} placeholder="Password"/>
                    </div>
                    <div style={{width:'150px'} }>
                    <button style={{height:'40px'} }className="btn btn-primary" onClick={togglePassword}>
                     { passwordType==="password"? (<h6>show</h6>) :(<h6>hide</h6>) }
                     </button>
                    </div>
                    </div>
                     </div>
                    
                <div className="confirm-password">
                    <label className="form-label" htmlFor="confirmPassword">Confirm Password </label>
                    <div style={{display:'flex'}}>
                    <div style={{width:'1000px'} }>
                    <input className="form-control" type={confirm} id="confirmPassword" value={confirmPassword} onChange = {(e) => handleInputChange(e)} placeholder="Confirm Password"/>
                    </div>
                    <div style={{width:'150px'} }>
                    <button style={{height:'40px'} }className="btn btn-primary" onClick={showConfirm}>
                     { confirm==="password"? (<h6>show</h6>) :(<h6>hide</h6>) }
                     </button>
                    </div>
                    </div>
                </div>
            </div>
            <div className="footer">
                <button type="submit" className="btn btn-primary">Register</button>
            </div>
        </div>
        </form>
       
    )       
}

export default RegistrationForm