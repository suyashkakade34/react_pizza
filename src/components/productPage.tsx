import { Table } from "react-bootstrap";
import { Order } from "../utils/Order";
import { useEffect, useState } from "react";
import axios from "axios";
import '../components/style.css'
import { OrderDetails } from "../utils/OrderDetails";
import PopupMessage from "../utils/PopupMessage";
import { Pizza } from "../utils/Pizza";

interface ProOrderProps{
  order:(OrderDetails:OrderDetails)=> void;
}



const ProductPage:React.FC<ProOrderProps>=({order})=> {

  const [showPopup, setShowPopup] = useState(false);
  const [editable, setEditable] = useState<boolean>(true);
  const [timeRemaining, setTimeRemaining] = useState<number>(15 * 60);
  const [data,setData]=useState<Order[]>([]);
  const [selectedSizes, setSelectedSizes] = useState<string[]>([]);
const [selectedQuantities, setSelectedQuantities] = useState<number[]>([]);
const [selectPrice,setSelectPrice]=useState<number[]>([])
const [pizza,setPizza]=useState<Pizza>()



  
  useEffect(() => {
    let timer: NodeJS.Timeout;

    if (timeRemaining > 0 ) {
      timer = setInterval(() => {
        setTimeRemaining(prevTime => Math.max(prevTime - 1, 0)); // Ensure timer never goes below 0
      }, 1000);
    } else {
      setEditable(false)
    }

    return () => {
      clearInterval(timer);
    };
  }, [timeRemaining]);




  //To update the pizza within 15 minutes
  const handleUpdateOrder = (index: number) => {
    
    const selectedPizza = data[index];
    const updatedPizza = selectedPizza.pizza.map((pizza, pizzaIndex) => ({
      ...pizza,
      size: selectedSizes[pizzaIndex],
      quantity: selectedQuantities[pizzaIndex],
      price:selectPrice[pizzaIndex]*selectedQuantities[pizzaIndex]
    }));
    const updatedOrder={
      ...selectedPizza,
    
      pizza:updatedPizza,
      totalAmount:parseInt(sumOfMultiplications.toFixed())
    }
    console.log(selectedPizza);
    
    console.log(updatedOrder);
    
    axios
      .put(`http://localhost:8080/orders/${selectedPizza.orderId}`, updatedOrder)
      .then(response => {
        // Handle success, maybe show a success message
        console.log('Order updated:', response.data.data);
        setShowPopup(true);
    setTimeout(() => {
      setShowPopup(false)   
    }, 3000);
   
      })
      .catch(error => {
        // Handle error
        console.error('Error updating order:', error);
      });
  }
  
  const handleDeleteOrder = (index: number) => {
    setShowPopup(true);
    setTimeout(() => {
      setShowPopup(false)   
    }, 3000);
   
  
    axios
      .delete(`http://localhost:8080/orders/${index}`)
      .then(response => {
        showPopup && <PopupMessage message="Order has been updated" />
      })
      .catch(error => {
        // Handle error
        console.error('Error updating order:', error);
      });
  }



  
 //To fetch the order that is created
  async function fetchDataById() {
    try 
    {
        const response = await axios.get<{ data: Order[] }>("http://localhost:8080/orders/order");  
        setData(response.data.data);
    } catch (error) {
        console.error('Error fetching data:', error);
    }
};


useEffect(()=>{
  fetchDataById();
  
},[])

useEffect(() => {
  if (data.length > 0) {
    const initialSelectedSizes = data[0].pizza.map((pizza) => pizza.size);
    const initialSelectedQuantities = data[0].pizza.map((pizza) => pizza.quantity);
    const intialSelectPrice=data[0].pizza.map((pizza)=>(pizza.price/pizza.quantity))
    setSelectedSizes(initialSelectedSizes);
    setSelectedQuantities(initialSelectedQuantities);
    setSelectPrice(intialSelectPrice)
  }
}, [data]);
let sumOfMultiplications:number=0;
selectPrice.forEach((value1, index) => {
  const value2 = selectedQuantities[index];
  const multiplication = value1 * value2;
  sumOfMultiplications += multiplication;
});
//To send the data to the Bill component
const handleBill=async (orderId:number)=>{
  setTimeRemaining(0)
  setEditable(false)
  const apiUrl=`http://localhost:8080/orders/bill/${orderId}`
  try {
    const response = await axios.get<{ data: OrderDetails }>(`${apiUrl}`); 
    setEditable(true)
    order(response.data.data)
    showPopup && <PopupMessage message="Order Placed SuccessFully"/>
    
    
} catch (error) {
    console.error('Error fetching data:', error);
}



}
const pizzaData=async (pizzaId:number)=>{
  const response= await axios.get<{data:Pizza}>(`http://localhost:8080/pizzas/${pizzaId}`)
    setPizza(response.data.data)
  
}

   
  const handleSizeSelection = (event: React.ChangeEvent<HTMLSelectElement>, index: number,id:number) => {
    pizzaData(id)
    const newSize = event.target.value;
    setSelectedSizes(prevSizes => {
      const updatedSizes = [...prevSizes];
      updatedSizes[index] = newSize;
      return updatedSizes;
    });
    
    setSelectPrice(prevPrices =>{
      const updatedPrice=[...prevPrices]
      if(pizza!==undefined){
        if(selectedSizes[index]==='regular'){
          updatedPrice[index]=pizza.regularSizePrice
          console.log(`inside new regular price` + pizza.regularSizePrice);
          
        }else if (selectedSizes[index]==='medium') {
          updatedPrice[index]=pizza.mediumSizePrice
        }else if (selectedSizes[index]==='large') {
          updatedPrice[index]=pizza.largeSizePrice
        }
      }
     return updatedPrice;
    })
  };
  
 

const handleQuantityChange = (index: number, newQuantity: number) => {
  setSelectedQuantities(prevQuantities => {
    const updatedQuantities = [...prevQuantities];
    updatedQuantities[index] = newQuantity;
    return updatedQuantities;
  });
};

const getTimeColorClass = (time: number): string => {
  if (time < 5) {
    return "red";
  } else if (time < 10) {
    return "orange";
  } else {
    return "green";
  }
};

    return (
        <div>
         
            <Table >
                <thead>
                    <tr>
                        <th >OrderId</th>
                        <th >Address</th>
                        <th >Pizza details</th>
                        {(timeRemaining>0)?(<th>Update</th>):(<th>Show Bill</th>)}
                        <th >Total Price</th>
                        <th><p>Time remaining: </p></th>
                    </tr>
                </thead>
                <tbody >
                {data.length>0 &&data.map((order,index) => (
          <tr key={order.orderId}>
            <td >{order.orderId}</td>
            <td >{order.address}</td>
            
            <td>
              <table >
                <thead>
                  <tr >
                    <th>Pizza id</th>
                    <th>Pizza Size</th>
                    <th>Pizza Quantity</th>
                    <th>Sub Price</th>
                  </tr>
                </thead>
                <tbody>
                  {order.pizza.map((pizzas,index) => (
                    <tr key={pizzas.pizzaId} >
                      <td >{pizzas.pizzaId}</td>
                      <td >
                        {(timeRemaining>0)?(<>
                          <label htmlFor={`sizeSelection${index}`}>Size:</label>
                      <select
                          name={`sizeSelection${index}`}
                          id={`sizeSelection${index}`}
                          value={selectedSizes[index]}
                          onChange={event => handleSizeSelection(event, index,pizzas.pizzaId)}
                        >
                          <option value="regular">Regular</option>
                          <option value="medium">Medium</option>
                          <option value="large">Large</option>
                        </select></>):(<span>{selectedSizes[index]}</span>)}
                        </td>
                      <td>
                        {(timeRemaining>0)?(<><div style={{ display: 'flex', justifyContent: 'center' }}>
                         <button onClick={() => handleQuantityChange(index, selectedQuantities[index] - 1)}>-</button>
                         <span>{selectedQuantities[index]}</span>
                         <button onClick={() => handleQuantityChange(index, selectedQuantities[index] + 1)}>+</button>
                        </div>
                        </>):(<span>{selectedQuantities[index]}</span>)}
                      </td> 
                      <td >{(selectPrice[index]*selectedQuantities[index]).toFixed()}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </td>
            <td> {(timeRemaining > 0 && editable) ? (
                     <>
                      <button
    className="btn btn-primary"
    onClick={() => handleUpdateOrder(index)}
    disabled={!editable}
  >
    {showPopup && <PopupMessage message="Order has been updated" />}
    Update Order
  </button>
  <br />
  <br />
  <button className="btn btn-danger" onClick={()=>handleDeleteOrder(order.orderId)}>Cancel Order</button>
  </>
  ):(
    <button className="btn btn-secondary" onClick={()=>handleBill(order.orderId)}>Show Bill</button>)}
      <br /><br />
      {(timeRemaining>0)?(<button className="btn btn-secondary" onClick={()=>handleBill(order.orderId)}>Show Bill</button>):(showPopup && <PopupMessage message="Order has been updated" />)}
      </td>
            <td >{sumOfMultiplications.toFixed()}</td>
            <td className="timer-cell">
            <span className={`minutes ${getTimeColorClass(Math.floor(timeRemaining / 60))}`}>
        {Math.floor(timeRemaining / 60)}
      </span>
      :
      <span className={`seconds ${getTimeColorClass(Math.floor(timeRemaining / 60))}`}>{timeRemaining % 60}</span>
</td>
          </tr>
          
        ))}
        
                </tbody>
            </Table>  
        </div>
    )
}



export default ProductPage;