import { Table } from "react-bootstrap";
import { Order } from "../utils/Order";
import { useEffect, useState } from "react";
import axios from "axios";
import '../components/style.css'
import { OrderDetails } from "../utils/OrderDetails";

interface ProOrderProps{
  order:(OrderDetails:OrderDetails)=> void;
}



const LastOrderProPage:React.FC<ProOrderProps>=({order})=> {

 


    const [data,setData]=useState<Order[]>([]);

  async function fetchDataById() {
    try {
        const response = await axios.get<{ data: Order[] }>("http://localhost:8080/orders/order");  
        setData(response.data.data);
        console.log(data);
        
        
    } catch (error) {
        console.error('Error fetching data:', error);
    }
};

const handleBill=async (orderId:number)=>{
  const apiUrl=`http://localhost:8080/orders/bill/${orderId}`
  try {
    const response = await axios.get<{ data: OrderDetails }>(`${apiUrl}`);  
    order(response.data.data)
    console.log(response.data.data);
    
    
    
} catch (error) {
    console.error('Error fetching data:', error);
}

}

useEffect(()=>{
    fetchDataById();
  },[])

  

    return (
        <div>
         
            <Table >
                <thead>
                    <tr>
                        <th >OrderId</th>
                        <th >Address</th>
                        <th >Pizza details</th>
                        <th >Total Price</th>
                        <th>View Bill</th>
                    </tr>
                </thead>
                <tbody >
                {data.map((order) => (
          <tr key={order.orderId}>
            <td >{order.orderId}</td>
            <td >{order.address}</td>
            
            <td>
              <table >
                <thead>
                  <tr >
                    <th>Pizza id</th>
                    <th>Pizza Size</th>
                    <th>Pizza Quantity</th>
                    <th>Sub Price</th>
                  </tr>
                </thead>
                <tbody>
                  {order.pizza.map((pizzas) => (
                    <tr key={pizzas.pizzaId} >
                      <td >{pizzas.pizzaId}</td>
                      <td >{pizzas.size}</td>
                      <td >{pizzas.quantity}</td>
                      <td >{pizzas.price}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </td>
            <td >{order.totalAmount}</td>

            <td><button className="btn btn-primary" onClick={()=>handleBill(order.orderId)}>Generate Bill</button></td>
          </tr>
          
        ))}
        
                </tbody>
            </Table>  
        </div>
    )
}


export default LastOrderProPage;