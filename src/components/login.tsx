import React, { useEffect, useState } from 'react'
import './login.css'
import {  Container} from 'react-bootstrap';
import axios from 'axios';
import { Customer } from '../utils/Customer';
 const Login:React.FC=()=>{
    
    const [info,setInfo]=useState<Customer[]>([])
    const [errorMessages, setErrorMessages] = useState({});
  const [isSubmitted, setIsSubmitted] = useState(false);

    const customers=async () => {
         axios
        .get<{data:Customer[]}>("http://localhost:8080/customers")
        .then(response=>{
            setInfo(response.data.data)
            console.log(response.data.data);
            
        }).catch(error=>{
            console.log(error);
            
        })
    }
    useEffect(()=>{
        customers();
        if (info.length>0) {
            setInfo(info)
        }
    },[])

    const errors = {
        uname: "invalid username",
        pass: "invalid password"
      };
    const handleSubmit = (event: { preventDefault: () => void; }) => {
        //Prevent page reload
        event.preventDefault();
        
    
        var { phone, pass } = document.forms[0];
    
        // Find user login info
        const userData = info.find((user) => user.phoneNumber === phone.value);
    
        // Compare user info
        if (userData) {
          if (userData.password === pass.value) {
            // Invalid password
            setIsSubmitted(true);
            
          } else {
            setErrorMessages({ name: "pass", message: errors.pass });
            console.log('password invalid');
            
          }
        } else {
          // Username not found
          console.log('username invalid');
          
          setErrorMessages({ name: "uname", message: errors.uname });
        }
      };
    
const renderform=(
    <section className="h-50 gradient-form" style={{backgroundColor:'#eee'}}>
  <div className="container h-100">
    <div className="row d-flex justify-content-center align-items-center h-100">
      <div className="col-xl-10">
        <div className="card rounded-3 text-black">
          <div className="row g-0">
            <div className="col-lg-6">
              <div className="card-body ">

                <div className="text-center">
                  <img src={`https://cdn.sanity.io/images/kts928pd/production/5f40b88897cffbe8dd6a68094fe12ff71b53c1eb-731x731.jpg`}
                    style={{width: "185px"}} alt="logo"/>
                  <h4 className="mt-1 mb-5 pb-1">We are The Suyshaa's Team</h4>
                </div>

                <form onSubmit={handleSubmit}>
                  <p>Please login to your account</p>

                  <div className="form-outline mb-4">
                    <input type="text" id="form2Example11" name='phone' className="form-control"
                      placeholder="Phone number " />
                    <label className="form-label" htmlFor="form2Example11">Username</label>
                  </div>

                  <div className="form-outline mb-4">
                    <input type="password" id="form2Example22" name='pass' className="form-control" />
                    <label className="form-label" htmlFor="form2Example22">Password</label>
                  </div>

                  <div className="text-center pt-1 mb-5 pb-1">
                    <button className="btn btn-primary btn-block fa-lg gradient-custom-2 mb-3" type="submit">Log
                      in</button>
                      {isSubmitted ? <div>User is successfully logged in</div> : <div>Login Failed</div>}
                    <a className="text-muted" href="#!">Forgot password?</a>
                  </div>

                  <div className="d-flex align-items-center justify-content-center pb-4">
                    <p className="mb-0 me-2">Don't have an account?</p>
                    <a href="/signup"><button type="button" className="btn btn-outline-danger">Create new</button></a>
                  </div>

                </form>

              </div>
            </div>
            <div className="col-lg-6 d-flex align-items-center gradient-custom-2">
              <div className="text-white px-3 py-4 p-md-5 mx-md-4">
                <h4 className="mb-4">people disappoint, but pizza never does</h4>
                <p className="small mb-0">
                Pizza was made for television in so many ways: it is easy to heat up, easy to divide and easy to eat in a group. It is easy to enjoy, easy to digest and easy-going. It is so Indian!
                <br />  
                -suyash
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
)

    return(
        <div>
            <Container style={{marginTop:"50",paddingTop:'70px',paddingBottom:"50px",backgroundColor:"#EEE"}}>   
            <h3>Login Page</h3> 
        {isSubmitted ? ( window.location.href = '/' ) : ( renderform)}
               </Container> 
        </div>
    )
 }
 export default Login;