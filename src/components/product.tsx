import React, { useState, useEffect } from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { Pizza } from '../utils/Pizza';
import "./underline.css"
import PopupMessage from '../utils/PopupMessage';
interface ProductProps{
  pizza:Pizza;
  cartItem: (pizza: Pizza, size: string,quantity:number,price:number) => void;
}

const Product :React.FC<ProductProps>=({pizza,cartItem})=>{
  const [quantity,setQuantity]=useState(0);
  const [selectedSize,setSelectedSize]=useState("")
  const [pizzaPrice,setPizzaPrice]=useState(0)
  const [isHovered, setIsHovered] = useState(false);

  const handleIncrease=()=>{
    setQuantity((prevQuantity)=>prevQuantity+1);
  }

  const handleDecrease=()=>{
    if(quantity>0){
    setQuantity((prevQuantity)=>prevQuantity-1);
    }else{
      alert("quantity can't be negative")
    } 
  }
  
  useEffect(()=>{
    switch(selectedSize){
      case"regular":
        setPizzaPrice(pizza.regularSizePrice)
        break;
      case"medium":
        setPizzaPrice(pizza.mediumSizePrice)
        break;
      case"large":
        setPizzaPrice(pizza.largeSizePrice)
        break;  
      default:
        setPizzaPrice(0)
    }
    
  },[selectedSize,pizza])
  
  const [showPopup,setShowPopup]=useState(false)
  

  const handleCart = () => {
    
    if(quantity){
      if(selectedSize){ cartItem(pizza, selectedSize,quantity,pizzaPrice);
        setShowPopup(true);
        setTimeout(() => {
          setShowPopup(false);
        }, 3000);
      switch (selectedSize) {
        case "Regular":
          setPizzaPrice(pizza.regularSizePrice);
          break;
        case "Medium":
          setPizzaPrice(pizza.mediumSizePrice);
          break
        case "Large":
          setPizzaPrice( pizza.largeSizePrice);
          break;
        default:  
          setPizzaPrice(0);
     }  
      // cartItem(pizza, selectedSize,quantity,pizzaPrice);
    }
    else{
      alert('Please select a size before adding to cart.');
    } 
    } else{
      alert('Quantity cannot be zero')
    }
};

  const handleSizeSelection = (event: React.ChangeEvent<HTMLSelectElement>) => {
    // console.log(event.target.value);
     setSelectedSize(event.target.value);
    }
    const timerStyles={
      padding: '10 px',transform: isHovered ? 'scale(1.08)' : 'scale(1)',
      transition: 'transform 0.3s ease-in-out'
    
    }
  
  return (
    <div style={{display:'flex', height:'330px',justifyContent:'flex-start'}}>
        <Card style={timerStyles}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
         key={pizza.pizzaId} >
          <Card.Img variant="top" height={'150px'} src={pizza.imageUrl} />
          <Card.Body>
            <Card.Title style={{fontSize:'20px'}}>{pizza.pizzaName}</Card.Title>
            <Card.Text>
            <div style={{display:'flex',justifyContent:'space-between'}}>
            <div style={{justifyContent:'center',marginTop:'20px'}}>
                <select className='dropdown' name="sizes" id="size" onChange={handleSizeSelection}>
                    <option className='btn btn-light dropdown-toggle'  selected  value="">size</option>
                    <option className='btn btn-light dropdown-toggle'value="regular"> Regular:{pizza.regularSizePrice}</option>
                    <option className='btn btn-light dropdown-toggle'value="medium"> Medium:{pizza.mediumSizePrice}</option>
                    <option className='btn btn-light dropdown-toggle'value="large"> Large:{pizza.largeSizePrice}</option>               
                </select>
              </div>
                <div style={{display:'flex'}}>
                  <Button variant="outline-primary" style={{marginTop:'20px' ,height:'25px'}}  onClick={handleDecrease}>-</Button>
                  <span style={{marginTop:'20px',height:'2px'}}>{quantity}</span>
                  <Button variant='outline-primary' style={{marginTop:'20px',height:'25px'}} onClick={handleIncrease}>+</Button>
                </div> 
              </div>
            </Card.Text>
            <Card.Footer>
              <Button variant="primary" className='cart' onClick={handleCart}>Add to cart</Button>
              {showPopup && <PopupMessage message="Pizza Added to the cart"/>}
            </Card.Footer>     
        </Card.Body>
        </Card>
    </div>
  );
}

export default Product;
