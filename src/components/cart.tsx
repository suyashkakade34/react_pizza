import { useState } from 'react';
import { CartItem } from '../utils/cartItem';
import { Table,Button,Card,ListGroup } from 'react-bootstrap';
import { Order } from '../utils/Order';
import axios from 'axios';
import PopupMessage from '../utils/PopupMessage';

interface CartProps{
  cartItems: CartItem[]  ;
  removeCartItem:(pizzaId: number, selectedSize: string)=>void;
  updateQuantity:(pizzaId: number, quantity: number,selectedSize:string)=>void
  removeAll:()=>void;
}




const Cart:React.FC<CartProps>=({cartItems,removeCartItem,updateQuantity,removeAll})=>{

  const [showPopup,setShowPopup]=useState(false)
  const calculateTotalAmount = () => {
    
    let subtotal = 0;
    cartItems.forEach((item) => {
      subtotal += item.selectedPrice * item.quantity;
    });
    return subtotal;
  };
  const createOrder = async () => {
   

    try {
      const orders: Order = {
        customerId: 3,
        address: 'pune',
        totalAmount:calculateTotalAmount(),
        orderId:0,
        date:"",
        pizza: cartItems.map((item) => ({
          pizzaId: item.pizzaId,
          quantity: item.quantity,
          size: item.selectedSize,
          price: item.selectedPrice * item.quantity,
          image:item.imageUrl
        })),
      };
     
      setTimeout(() => {
        setShowPopup(true)
      }, 3000);
    const response = await axios.post('http://localhost:8080/orders', orders);
      
    //if order inserted successfully then 
    if(response){
      
        window.location.href = '/cart'; 
      
    removeAll(); 
    
      
    }
  } catch (error) {
    
    console.error('Error creating order:', error);
  }
};

const handleQuantityChange = (pizzaId: number, quantity: number,selectedSize:string) => {     updateQuantity(pizzaId, quantity, selectedSize);      };


  return (
    <Card style={{ position: 'sticky', height:'600px', top: '150px', right: '40px', width: '400px', maxHeight: 'calc(75vh - 150px)', overflowY: 'auto' ,border: '1px solid #ccc' }}>
    <Card.Body style={{ paddingBottom: "70px", position: "relative" }}>
      <Card.Title>Your Cart</Card.Title>
      {cartItems.length === 0 ? (
        <Card.Text>
          <img src={'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQC4Fdpa_78ctqismsYzJZQnOzoUbTWS7izdQUd6NvWRQ&s'} alt="" />
          <h5>I am Empty and Your Stomach too... common we both need pizza</h5>
        </Card.Text>
      ) : (
          <div>
                <Table  bordered-primary hover>
              <thead style={{backgroundColor:'grey'}}>
                <tr>
                  <th>Image</th>
                  <th>Name</th>
                  <th>Size</th>
                  <th>Quantity</th>
                  <th>Price</th>
                  <th>Total</th>
                  <th>Remove</th>
                </tr>
              </thead>
              <tbody>
                {cartItems.map((item) => (
                  <tr key={item.pizzaId}>
                    <td>
                      <img src={item.imageUrl} alt={item.pizzaName} style={{ width: '80px' }} />
                    </td>
                    <td>{item.pizzaName}</td>
                    <td>{item.selectedSize}</td>
                    <td>
                      <div style={{display:'flex'}}><Button variant="outline-primary cart" onClick={() => handleQuantityChange(item.pizzaId, item.quantity - 1, item.selectedSize)}>-</Button>
                      <span className="mx-2">{item.quantity}</span>                      
                      <Button variant="outline-primary cart" onClick={() => handleQuantityChange(item.pizzaId, item.quantity + 1, item.selectedSize)}>+</Button></div>
                    </td>
                    <td>&#8377;{item.selectedPrice}</td>
                    <td>&#8377;{item.selectedPrice * item.quantity}</td>
                    <Button  variant='outline-primary cart' onClick={()=>{removeCartItem(item.pizzaId,item.selectedSize)}}>Remove</Button>
                    {showPopup && <PopupMessage message="Pizza removed from the cart"/>}
                  </tr>
                ))}
              </tbody>
              
             </Table>
            <div  style={{position:'sticky'}}>
            <ListGroup className="list-group list-group-flush" >
              <ListGroup.Item as="li">Sub Total:  &#8377;{calculateTotalAmount()}</ListGroup.Item>
            </ListGroup>
            </div>
            <Button className='btn btn-primary cart' onClick={createOrder}> 
            {showPopup && <PopupMessage message="Order has been updated" />}
          Create Order
        </Button>
          </div>
      )
        }
      
      </Card.Body>
    </Card>
  );
}

export default Cart;