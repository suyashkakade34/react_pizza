export interface BillUtil{
    quantity:number;
    size:string;
    subTotal:number;
    pizzaName:string;
    pizzaId:number;
    imageUrl:string;
}