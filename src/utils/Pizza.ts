export interface Pizza {
    pizzaId: number;
    pizzaName: string;
    description: string;
    type: string;
    imageUrl: string;
    regularSizePrice:number;
    mediumSizePrice:number;
    largeSizePrice:number;
  }