

export interface CartItem{
    pizzaId: number;
    pizzaName: string;
    imageUrl: string;
    selectedSize:string;
    quantity:number;
    selectedPrice:number;
}