import { BillUtil } from "./BillUtil";

export interface OrderDetails{
    orderId:number;
    pizza:BillUtil[];
    totalAmount:number;
    date:string;
    address:string;
    firstName:string;
    lastName:string;
}