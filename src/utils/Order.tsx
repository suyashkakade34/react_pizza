import { orderdPizza } from "./orderedPizza";
export interface Order{
    customerId:number;
    address:string;
    totalAmount:number;
    orderId:number,
    date:string;
    pizza:orderdPizza[];
}