export interface Customer{
    customerId:number;
    firstName:string;
    lastName:string;
    address:string;
    phoneNumber:number;
    emailId:string;
    password:string;
}