export interface orderdPizza{
    pizzaId:number;
    size:string;
    image:string;
    quantity:number;
    price:number;
}