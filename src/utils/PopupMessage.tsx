import React from 'react';
import "./popup.css"
interface PopupMessageProps {
    message: string;
  }
  
  const PopupMessage: React.FC<PopupMessageProps> = ({ message }) => {
    return (
      <div className="popup-message">
        {message}
      </div>
    );
  };
  
  export default PopupMessage;