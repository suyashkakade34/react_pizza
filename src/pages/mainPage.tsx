import { Container,Row,Col, Card } from "react-bootstrap";
import Product from "../components/product";
import Cart from "../components/cart";
import { Pizza } from "../utils/Pizza";
import { useState,useEffect } from "react";
import { CartItem } from "../utils/cartItem";
import axios from "axios";
import PizzaNa from "../components/PizzaNA";
 
const MainPage = () => {
    const [pizza, setPizza] = useState<Pizza[]>([]);
    const [cartItems,setCartItems]= useState<CartItem[]>([]);
    const [pizzaType, setPizzaType] = useState<'Veg' | 'Non-Veg' | 'all'>('all'); 
    const [search,setSearch]=useState(false)
    const [searchQuery, setSearchQuery] = useState('');
    
    
    const filteredPizzas = pizza.filter(pizzas => {
      const matchesType = pizzaType === 'all' || pizzas.type === pizzaType;
      const matchesSearch = pizzas.pizzaName.toLowerCase().includes(searchQuery.toLowerCase());
      return matchesType && matchesSearch;
    });
    
    //fetch pizza from the backend
    async function fetchPizza() {
        try {
          const response = await axios.get<{ data: Pizza[] }>("http://localhost:8080/pizzas");
          setPizza(response.data.data); 
        } catch (error) {
          console.error("Error fetching pizza data:", error);
        }
      }
    useEffect(()=> {
        fetchPizza()
    },[])

    const updateQuantity = (pizzaId: number, quantity: number, selectedSize: string) => {    if (quantity < 1) {      removeCartItem(pizzaId, selectedSize);    } else {      setCartItems((prevItems) =>        prevItems.map((item) =>          item.pizzaId === pizzaId && item.selectedSize === selectedSize ? { ...item, quantity: quantity } : item        )      );    }    console.log(selectedSize);  };
    
//take cartItem type of input from the user and then pass it to the cart component
const addToCart = (pizza: Pizza, selectedSize: string, quantity: number, selectedPrice: number) => {

    const existingPizza = cartItems.find((item) => item.pizzaId === pizza.pizzaId && selectedSize === item.selectedSize);


    //if it finds

    if (existingPizza) {
      const updatedItems = cartItems.map((item) => {
        if (item.pizzaId === existingPizza.pizzaId && selectedSize === item.selectedSize) {
          return { ...item, quantity: quantity + item.quantity, selectedPrice:item.selectedPrice,  selectedSize };
        }
        return item;
      });

      setCartItems(updatedItems);
    } else {  
      const newPizza = { ...pizza, selectedSize: selectedSize || '', quantity, selectedPrice };

    setCartItems((prevItems) => [...prevItems,newPizza]);
    }
 
      
    
  };
  const removeCartItem = (pizzaId: number, selectedSize: string) => {
    setCartItems((prevItems) => prevItems.filter((item) => item.pizzaId !== pizzaId || item.selectedSize !== selectedSize));
  };

  const removeAll=()=>{
    setCartItems([])
  }

  const handleTypeSelection = (event: React.ChangeEvent<HTMLInputElement>) => {
    (event.target.value==='all')?(
         setPizzaType("all")
    ):(event.target.value==='Veg')?(setPizzaType("Veg")):(event.target.value==='Non-Veg')?(setPizzaType("Non-Veg")):(setPizzaType("all"))
}
  
      
    return(
     <Container style={{paddingTop:"75px",backgroundColor:"#EEE"}}>
            <Row >
                <Col xs sm={12} lg={12}>
                    <div >
                  
                    <label htmlFor="selectType">SelectType</label>
                    <input
  type="radio"
  name="foodType"
  value="all"
  style={{ marginLeft: '15px' }}
  checked={pizzaType === 'all'}
  onChange={handleTypeSelection}
/>All
<input
  name="foodType"
  type="radio"
  value="Veg"
  style={{ marginLeft: '25px' }}
  checked={pizzaType === 'Veg'}
  onChange={handleTypeSelection}
/>Veg
<input
  name="foodType"
  type="radio"
  value="Non-Veg"
  style={{ marginLeft: '25px' }}
  checked={pizzaType === 'Non-Veg'}
  onChange={handleTypeSelection}
/>Non-Veg

<label htmlFor="search" style={{marginLeft:'60px'}}>Search Pizzas</label>
                    <input
  value={searchQuery}
  onChange={e => {
    setSearchQuery(e.target.value);
    setSearch(!!e.target.value); // Set search to true if value is not empty
  }}
  placeholder="Search Pizzas..."
  style={{marginBottom:'10px',marginLeft:'10px'}}
 
/>
<br /><br />
                    </div>
                </Col>
    
              
               
         
                <Col xs sm={8} lg={8}>
                    <Card style={{backgroundColor:"#EEE"}}>
                        <Row>
                            {(filteredPizzas.length>0)?(<>{(!search)?(<>{filteredPizzas.map(pizzas=>(
                            <Col key={pizzas.pizzaId}>
                                <Product pizza={pizzas} cartItem={addToCart} />
                            </Col>
                            ))}</>):(<> {filteredPizzas.map(pizzaItem => (
                              <Card key={pizzaItem.pizzaId}>
                                <Product pizza={pizzaItem} cartItem={addToCart} />
                              </Card> 
                            ))}</>)}</>):(<>
                            <Col>
                              <PizzaNa/>
                            </Col>
                            </>)}
                        </Row>
                    </Card>
                </Col>
                <Col  xs sm={4} lg={4} >
                    <Card  style={{backgroundColor:"#EEE"}}>
                        <Cart updateQuantity={updateQuantity} cartItems={cartItems} removeCartItem={removeCartItem} removeAll={removeAll}/>
                    </Card>
                </Col>
                              
            </Row>
    </Container>
    );
}

export default MainPage;