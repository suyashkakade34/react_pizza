import { Container,Row,Col, Card } from "react-bootstrap";
import ProductPage from "../components/productPage";
import Bill from "../components/bill";
import { useState } from "react";
import { OrderDetails } from "../utils/OrderDetails";

function MainCart() {
    const [currentOrder,setData]=useState<OrderDetails>();
    const addToBill=(order:OrderDetails)=>{
        setData(order)
    }    
    
    return (
      <div>
         <Container style={{marginTop:"100",paddingTop:"60px",backgroundColor:"#EEE"}}>
            <Row >
                <Col xs sm={8} lg={8}>
                    <h3>Order</h3>
                </Col>
                <Col  xs sm={4} lg={4}>
                    <div>
                       <h4>Payment Form</h4>
                    </div>
                </Col>
                <Col xs sm={8} lg={8}>
                    <Card style={{backgroundColor:"#EEE"}}>
                        <Row>
                            
                            <Col>
                                <ProductPage order={addToBill}/>
                            </Col>
                        </Row>
                    </Card>
                </Col>
                <Col  xs sm={4} lg={4} >
                        <Card  style={{backgroundColor:"#EEE"}}>
                        
                            <Bill data={currentOrder}/>
                        
                    </Card>
                </Col>
            </Row>
            </Container>
      </div>
    );
  }
  
  export default MainCart;