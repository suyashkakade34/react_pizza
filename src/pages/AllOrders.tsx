import { Container,Row,Col, Card } from "react-bootstrap";
import AllProductPage from "../components/AllProductPage";
import Bill from "../components/bill";
import { OrderDetails } from "../utils/OrderDetails";
import { useState } from "react";
const AllOrders=()=> {
    const [currentOrder,setData]=useState<OrderDetails>();
    const addToBill=(order:OrderDetails)=>{
        setData(order)
    }    
    
    return (
      <div>
         <Container style={{marginTop:"100",paddingTop:"60px",backgroundColor:"#EEE"}}>
            <Row >
                <Col xs sm={8} lg={8}>
                    <h3>All Orders</h3>
                </Col>
                <Col xs sm={4} lg={4}>
                    <h3>Final Bill</h3>
                </Col>
                <Col xs sm={8} lg={8}>
                    <Card style={{backgroundColor:"#EEE"}}>
                        <Row> 
                            <Col>
                            <AllProductPage order={addToBill}/>
                            </Col>
                        </Row>
                    </Card>
                </Col>
                <Col xs sm={4} lg={4}>
                    <Card style={{backgroundColor:"#EEE", // position:'-webkit-sticky',
           position:'sticky',
          top:60,
          padding:'10px',
          textAlign:'center'}}>
                        <Row> 
                            <Col>
                            <Bill data={currentOrder}/>
                            </Col>
                        </Row>
                    </Card>
                </Col>
            </Row>
            </Container>
      </div>
    );
  }
  
  export default AllOrders;