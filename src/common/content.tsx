import {
    BrowserRouter,
    Routes,
    Route,
  } from "react-router-dom";
  import MainPage from "../pages/mainPage";
import MainCart from "../pages/mainCart";
import LastOrder from "../components/LastOrder";
import AllOrders from "../pages/AllOrders";
import Login from "../components/login";
import RegistrationForm from "../components/RegistrationnForm";
const Content = () => {
    return(
    <>
        <BrowserRouter>
            <Routes>
                <Route path="/" Component={MainPage}>
                </Route>
                <Route path="/lastOrder" Component={LastOrder}>
                </Route>
                <Route path="/orders" Component={AllOrders}/>
                <Route path="/cart" Component={ MainCart}/>
                <Route path="/login" Component={Login}/>
                <Route path="/signup" Component={RegistrationForm}/>
            </Routes>
        </BrowserRouter>
    </>
    )
}

export default Content;