
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';  
const NavbarComponent = () => {

    return  <Navbar fixed="top" style={{height:'60px'}} className="bg-body-tertiary">
                <Container>
                    <Navbar.Brand href="/" style={{marginTop:'5px'}}>
                    <img height={'35px'} src={'https://cdn.sanity.io/images/kts928pd/production/5f40b88897cffbe8dd6a68094fe12ff71b53c1eb-731x731.jpg'} alt="" />
                    Suyshaa's PizzaHub    
                      </Navbar.Brand>
                    <Navbar.Toggle />
                    <div className="dropdown">
  <button
    className="btn btn-primary dropdown-toggle"
    type="button"
    id="dropdownMenuButton"
    data-mdb-toggle="dropdown"
    aria-expanded="false"
  >
    Order History
  </button>
  <ul className="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <li><a className="dropdown-item" href="/">Home</a></li>
    <li><a className="dropdown-item" href="/lastOrder">LastOrder</a></li>
    <li><a className="dropdown-item" href="/orders">All Orders</a></li>
  </ul>
</div>
                    <Navbar.Collapse className="justify-content-end">
                    
                        <Navbar.Text>
                        <a href="/login"><button className='btn btn-primary'>Login here</button></a>
                        </Navbar.Text>
                        
                    </Navbar.Collapse>
                </Container>
            </Navbar>
}

export default NavbarComponent;